import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Blog from "./views/Blog.vue";
import NotFound from "./components/NotFound.vue";
import Post from "./components/Post.vue";
import Contact from "./components/Contact.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    },
    {
      path: "/blog",
      name: "blog",
      component: Blog
    },
    {
        path: '/post/:id',
        name:'post',
        component: Post,
        props: true,
    },
      {
          path: "/contacts",
          name: "contacts",
          component: Contact
      },
     {
          path: "/404",
          component: NotFound,
          meta: {
              title: "404"
          }
     },
    { path: "*", redirect: "/404" }

  ]
});
