import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from 'axios'

Vue.config.productionTip = false;

new Vue({
    data () {
        return {
            message: "title",
            posts: null,
            endpoint: 'https://jsonplaceholder.typicode.com/posts/',
        }
    },
    created() {
       console.log("test");
    },
  router,
  store,
  render: h => h(App)
}).$mount("#app");
